// Controllers contain the functions and business logic of our Express JS app.
// Meaning all the operations it can perform will be written in this file.

const Task = require("../models/Task.js");

// Controller Functions:

// Funciton for getting all tasks
module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

// Function for creating tasks
module.exports.createTask = (reqBody) => {
  let newTask = new Task({
    name: reqBody.name,
  });

  return newTask.save().then((savedTask, error) => {
    if (error) {
      console.log(error);
      return false;
    } else if (savedTask != null && savedTask.name == reqBody.name) {
      return "Duplicate Task Found";
    } else {
      console.log(savedTask);
      return savedTask;
    }
  });
};

// Function for updating a task
module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    }
    result.name = newContent.name;

    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return false;
      } else {
        return updatedTask;
      }
    });
  });
};

// Function for delete a task
module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
    if (error) {
      return false; // "Cannot Delete Task" or "An Error has occured"
    } else {
      return deletedTask; // "Task deleted successfully!"
    }
  });
};

// Function for finding task by ID
module.exports.getOneTask = (taskId) => {
  return Task.find({ taskId }).then((foundTask, error) => {
    if (error) {
      return false;
    } else {
      return foundTask;
    }
  });
};

// Function to Update the status of a task
module.exports.updateTaskStatus = (taskId, newContent) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    }
    result.status = newContent.status;

    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return false;
      } else {
        return updatedTask;
      }
    });
  });
};
